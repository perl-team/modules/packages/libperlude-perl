Source: libperlude-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Mason James <mtj@kohaaloha.com>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libperlude-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libperlude-perl.git
Homepage: https://metacpan.org/release/perlude
Rules-Requires-Root: no

Package: libperlude-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Description: shell and powershell pipes, haskell keywords mixed with the awesomeness of perl
 Perlude: If you're used to a unix shell, Windows Powershell or any
 language coming with the notion of streams, perl could be frustrating
 as functions like map and grep only works with arrays.
 .
 The goodness of it is that '|' is an on demand operator that can easily
 compose actions on potentially very large amount of data in a very memory
 and you can control the amount of consummed data in a friendly way.
 .
 Perlude gives a better '|' to Perl: as it works on scalars which can be
 both strings (like unix shell), numbers or references (like powershell).
